import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  constructor(
    private router: Router,
  ) { }
  public purpose: string = 'Guest';

  selectPurpose(purpose: string) {
    this.purpose = purpose;
    // this.router.navigate(['/login'])
  }
  navigateToLogin() {
    console.log("PURPOSE", this.purpose)

  }
}
