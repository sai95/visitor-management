import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.visitormanagement.app',
  appName: 'Visitor Management',
  webDir: 'www',
  server: {
    url: "http://192.168.1.194:4200",
    cleartext: true
  }
};

export default config;
